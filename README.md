> Development for COSC 2.0 has begun. This repository is now archived.

# COSC Operating System
[![Gitter](https://badges.gitter.im/eli310chat/cosc.svg)](https://gitter.im/eli310chat/cosc?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

Home of COSC and it's software for ComputerCraft and CC: Tweaked.

Want to contribute? See the wiki for info to follow.
