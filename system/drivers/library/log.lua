--- An API for logging in computer craft, based of lua logging.
-- @classmod logging
-- @author Wobbo
-- @alias mt

local mt = {}
mt.__index = mt

--- The DEBUG level designates fine-grained informational events that are most useful to debug an application.
DEBUG = "DEBUG"

--- The INFO level designates informational messages that highlight the progress of the application at coarse-grained level.
INFO = "INFO"

--- The WARN level designates potentially harmful situations.
WARN = "WARN"

--- The ERROR level designates error events that still allow the application to continue running.
ERROR = "ERROR"

--- The FATAL level designates very severe error events that would lead the application to abort. 
FATAL = "FATAL"

--- A table to keep track of the order between levels. There is a field for each valid level. In order to be logged, the order of a level has to be equal to or greater than the current logging level.
-- @usage logging.order[logging.DEBUG] -- returns the order of DEBUG.
-- @field DEBUG The order of `logging.DEBUG`
-- @field INFO The order of `logging.INFO`
-- @field WARN The order of `logging.WARN`
-- @field ERROR The order of `logging.ERROR`
-- @field FATAL The order of `logging.FATAL`
order = {DEBUG = 1, INFO = 2, WARN = 3, ERROR = 4, FATAL = 5}

function mt.__gc(self) if self.file then self.file:close() end end

local function newArgs(func, file, format, level)
	local obj = setmetatable({}, mt)
	obj.func = func
	if file then
		local handle
		if not fs.exists(file) then
			handle = io.open(file, "w")
		else
			handle = io.open(file, "a")
		end
		obj.file = handle
	end
	obj.format = format or "[%date - %time] <%level> %message"
	obj:setLevel(level or INFO)
	return obj
end

--- Create a new logging object. 
-- Can also be called as `logging.new{func = func, file = file, format = format}`. Either a file or a function is advised, but not obligatory. 
-- @function new
-- @constructor
-- @tparam function func The function that is called when a message is logged. This function recieves a self argument, the level that is calles with and the message that is logged.
-- @string file The file that is logged to.
-- @string format The format that is used for logging to a file. The day can be inserted by `%date`, the time by using `%time`, the level by using `%level` and the message itself by using `%message`.
-- @string level The initial level for the `logging` object. 
-- @treturn logging The new logging object. 
-- @usage logger = logging.new{func = function(level, message)
--     print(level..": "..message) 
--   end,
--   file = "usage.log", 
--   format = "%date %time Usage: %level %message"}
-- @usage logger = logging.new(function(level, message) 
--   print(message) 
--   os.queueEvent("log Usage", level, message)
-- end)
-- @usage logger = logging.new{function(self, level, message) 
--   print(level..": "..message) end,
--   level = logging.DEBUG}
function new(...)
	local t
	if type(...) == "table" then
		t = ...
	else
		t = {...}
	end
	return newArgs(t.func or t[1], t.file or t[2], t.timeFmt or t[3], t.level or t[4])
end

--- Sets the minimum level needed for a message to be logged. Default is `INFO`.
-- @string level The minimum level.
-- @usage logger:setLevel(logging.WARN)
function mt:setLevel(level)
	if order[level] then 
		self.level = level 
	else 
		error("undefinded level "..level) 
	end 
end

--- Logs a message if the specified level is higher then the loggers level.
-- This function will call the 
-- @string level The level of the message.
-- @string message The message that need to be logged. 
-- @usage logger:log(logging.INFO, "Prepared the environment")
function mt:log(level, message)
	if order[level] < order[self.level] then return end
	if self.func then
		self:func(level, message)
	end
	if self.file then
		self.file:write(prepMsg(self.format, message, os.day(), textutils.formatTime(os.time(), true), level)..'\n')
		self.file:flush()
	end
end

--- Log a message with DEBUG level.
-- @string message The message to be logged. 
-- @usage logger:debug("Found the logging API")
-- @see log
-- @see DEBUG
function mt:debug(message)
	self:log(DEBUG, message)
end


--- Log a message with INFO level.
-- @string message The message to be logged.
-- @usage logger:info("Prepared the invironment")
-- @see log
-- @see INFO
function mt:info(message)
	self:log(INFO, message)
end

--- Log a message with WARN level.
-- @string message The message to be logged.
-- @usage logger:warn("Expected integer, got string")
-- @see log
-- @see WARN
function mt:warn(message)
	self:log(WARN, message)
end

--- Log a message with ERROR level.
-- @string message The message to be logged.
-- @usage logger:error("No modem found!")
-- @see log
-- @see ERROR
function mt:error(message)
	self:log(ERROR, message)
end

--- Log a message with FATAL level.
-- @string message The message to be logged.
-- @usage logger:fatal("No fuel found to refuel. Sending distress signal and aborting.")
-- @see log
-- @see FATAL
function mt:fatal(message)
	self:log(FATAL, message)
end


--- Prepare a message using a specified pattern.
-- @constructor
-- @tparam string pattern The pattern used for the message. Defaults to `"%date %time %level %message"`. The day can be inserted by `%date`, the time by using `%time`, the level by using `%level` and the message itself by using `%message`. 
-- @string message The message that needs to be formatted. The value for `%message`
-- @number date The value for `%date`.
-- @string time The value for `%time`. This needs to be formatted beforehand. 
-- @string level The value for `%level`.
-- @treturn string The prepared message.
function prepMsg(pattern, message, date, time, level)
	local logMsg = pattern or "[%date - %time] <%level> %message"
	message = string.gsub(message, "%%", "%%%%")
	logMsg = string.gsub(logMsg,"%%date", date)
	logMsg = string.gsub(logMsg, "%%time", time)
	logMsg = string.gsub(logMsg, "%%level", level)
	return string.gsub(logMsg, "%%message", message)
end
