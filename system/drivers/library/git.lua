expect = dofile("/rom/modules/cc/main/expect.lua").expect
glab = {}
ghub = {}
local function glab.getTree(id, path)
expect(id, "number")
expect(path, "string", "number")
h = http.get("https://gitlab.com/api/v4/projects/"..id.."/repository/tree?path="..tostring(path))
g = textutils.unsearialize(h.readAll())
h:close()
return g
end

return {
    lab = glab,
    hub = ghub
}
