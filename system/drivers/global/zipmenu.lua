local params = {...}
local cursor = 1
local selectedColor = colors.white
local titleColor = colors.white
local titleBgColor = colors.gray
local bgColor = colors.cyan
local txtColor = colors.white
local timerCount = nil
local menuExtraText = ""

local function centertext(text, y)
  local w, h = term.getSize()
  term.setCursorPos((w/2)-(#text /2), y)
  term.write(text)
 end

local function draw(title, menu, extra, s)
  local w, h = term.getSize()
  term.setBackgroundColor(bgColor)
  term.clear()
  local t1 = ""
  if not type(extra) == "number" then
    t1 = (#extra > 0 and extra or "")
  else
    t1 = extra
  end
  for i=1, #menu do
    if(i == cursor) then
      term.setTextColor(selectedColor)
      if not false then
      if not term.isColor() then
        centertext("["..menu[i].."]", (h/2)+i-(cursor-1))
      else
        centertext(" "..menu[i].." ", (h/2)+i-(cursor-1))
      end
      else
        p = 8 + ((i-(cursor-1))/5)
        term.setCursorPos(p + 4, (h/2)+i-(cursor-1))
        term.write("-> "..menu[i].."   ")
      end
    else
      if not false then
        term.setTextColor(txtColor)
        centertext(" "..menu[i].." ", (h/2)+i-(cursor-1))
      else
        p = 8 + ((i-(cursor-1))/5)
        term.setCursorPos(p + math.abs(2 ^ math.abs(cursor-i) / 40), (h/2)+i-(cursor-1))
        term.write(menu[i])
      end
    end   
  end
  term.setCursorPos((w/2)-(#title/2)-4,2)
  term.setBackgroundColor(titleBgColor)
  term.setTextColor(titleColor)
  write((" "):rep(#title+8))
  centertext(title, 2)
  term.setCursorPos(1, 1)
  term.clearLine()
  write(" " .. cursor .. "/" .. #menu .. " ") 
  term.setCursorPos(w - 1 - t1:len(), 1)
  term.write(t1)
  term.setCursorPos((w/2)-(#title/2)-4,2)
  term.setBackgroundColor(titleBgColor)
  term.setTextColor(bgColor)
  term.write(string.char(144))
  term.setCursorPos((w/2)+(#title/2)+4,2)
  term.setBackgroundColor(bgColor)
  term.setTextColor(titleBgColor)
  term.write(string.char(159))
end

function setColors(title, titleBg, bg, selTxt, txt)
    if txt == nil or title == nil or titleBg == nil or selTxt == nil or bg == nil then error("Specify 5 colors: Title text, Title background, Selection background, Selection text, and Regular Selection text", 2) end
    titleColor, titleBgColor, bgColor, selectedColor, txtColor = title, titleBg, bg, selTxt, txt
end

function setTimer(number, not_suppored) error("function no longer supported at this time. refer to the \'lua\" program on the shell for all available functions.") end

function drawMenuDrawer(title, menu, currentMenuDisplay)
  if type(currentMenuDisplay) ~= "table" then error("expected menu display in background as a table/array", 2) end
  t = term.current()
  w, h = term.getSize()
  w = window.create(t, 1, h, w, (h/5)*4)
  w1, h2 = w.getSize()
  p = 4
  title = currentMenuDisplay.title
  oc = cursor
  cursor = 1
  for i = 1, 5 do
    sleep(0.05)
    if type(t.redraw) == "function" then t.redraw() else staticDisplay(currentMenuDisplay.title, currentMenuDisplay.choices, "Select option v") end
    term.redirect(w)
    staticDisplay(title, menu)
    term.redirect(t)
    p = p - 1
    w.reposition(1, (h/5)+((h/5)*p))
  end
  term.redirect(w)
  drawMenu(title, menu)
  term.redirect(t)
  for i = 1, 5 do
    sleep(0.05)
    if type(t.redraw) == "function" then t.redraw() else staticDisplay(contentMenuDisplay.title, currentMenuDisplay.choices, "Select opion v") end
    term.redirect(w)
    staticDisplay(title, menu)
    term.redirect(t)
    p = p + 1
    w.reposition(1, (h/5)+((h/5)*p))
  end
  return cursor
end

function confirmAction(current, t, ...)
  if type(current) ~= "table" then error("table or window object is only supported for this parameter", 2) end
  if type(t) ~= "number" then error("number is only supported for this parameter", 2) end
  s = {t="",c={a="",d=""}}
  ex = {...}
  if t == 1 then
    s.t = "Confirm action..."
    s.c.a = "Confirm"
    s.c.d = "Cancel"
  elseif t == 2 then
    s.t = "Are you sure?"
    s.c.a = "Yes, I am sure"
    s.c.d = "No, go back"
  elseif t == 3 then
    if type(ex[1]) ~= "string" then error("string expected for question") end
    s.t = ex[1]
    s.c.a = "Yes"
    s.c.d = "No"
  elseif t == 4 then
    s.t = "Proceed? It may cause harm."
    s.c.a = "Yes, proceed."
    s.c.d = "No, cancel."
  else
    error("type not supported. check documentation for help.", 2)
  end
  c = drawMenuDrawer(s.t, s.c, current)
  if c == 1 then return true else return false end
end

function setCursorPos(n)
  if type(n) == "number" then cursor = n end
end

function getCursorPos()
  return cursor
end

function setExtraText(text)
  local t = (text and text or "")
  menuExtraText = t
  if type(t) == "text" or type(t) == "number" then else menuExtraText = "" end
end

function getExtraText()
  return menuExtraText
end

local cursorHighlight
function setHighlight(n) -- selects to the choice you want to highlight, resets after finish selecting at start of menu draw
if type(n) ~= "number" then error("expected a number to highlight") end
cursorHighlight = n
end

--<#SETTINGS#>--

-- table for settings
local settings = {
  visuals = {
    sonicMode = false,
  },
}

options = {}

-- creates a ui on-screen and shows available options

function options.showUI(startIn)
-- changed this to uid because ui-d looks like ui minus d to lua
  uid = {
    v = {
      av = true,
      theme = "tc",
      sm = false,
      rev = nil,
    },
    s = {
      av = false,
      theme = nil,
    }
  }
  q = false
  if fs.exists("/system/cosc32.lua") then end -- what's the point of this
  mm = true
  mf = "m"
  c = nil
  while not q do
    if mf == 'm' then
      c = drawMenu("Zipmenu Settings", "Visuals", "Sound")
      if c == 1 then
        if uid.v.av then
          mf = "v"
        else
          drawMenuDrawer("Feature unavailable", {"Ok"}, {title="Zipmenu Settings",choicss={"Visuals", "Sound"}})
        end
	  end
      if c == 2 then
        if uid.s.av then
          mf = "s"
        else
          drawMenuDrawer("Feature unavailable", {"Ok"}, {title="Zipmenu Settings",choices={"Visuals", "Sound"}})
        end
      end
    elseif mf == "v" then
      cthd = {
        av = false,
        ["Default"] = {
          tt = colors.white,
          tb = colors.gray,
          tn = colors.white,
          ts = colors.yellow,
          bg = colors.cyan
        },
        ["MS-DOS"] = {
          tt = colors.black,
          tb = colors.white,
          tn = colors.white,
          ts = colors.white,
          bg = colors.black,
        },
        ["More soon..."] = nil,
      }
	elseif mf == "s" then
      drawMenuDrawer("Access denied", {"Aww man..."}, {title="Zipmenu Settings",choices={"Visuals", "[X] Sound [X]"}})
      drawMenuDrawer("Feature unavailable", {"Ok"}, {title="Zipmenu Settings",choices={"Visuals", "Sound"}})
      drawMenuDrawer("Read this, then press enter", {"More tries will result", "in a computer lockdown.", "In about "..uptimeTries.."", "further block accesses,", "QuickMuffin8782's", "Betelguese Payload", "recreation will be", "injeted as a", "consenquence.", "", "For now, the session will", "exit."}, {title="Zipmenu Settings",choicss={"Visuals", "Sound"}})
      q = true
    end
    cth = chtd[uid.v.theme]
    c = drawMenu("Settings > Visuals", {"Back", "Theme", "Sonic Mania Mode " .. (uid.v.sm and "[ON] " or "[OFF]")})
    if c == 1 then
      tmp = confirmAction({title="Settings > Visuals",choices={"Back", "Theme", "Sonic Mania Mode " .. (uid.v.sm and "[ON] " or "[OFF]")}}, 3, "Save current settings?")
      if tmp then
        settings.visuals.sonicManiaMode = uid.v.sm
        mf = "m"
      else
        tmp = confirmAction({title="Settings > Visuals",choices={"Back", "Theme", "Sonic Mania Mode " .. (uid.v.sm and "[ON] " or "[OFF]")}}, 2)
        if tmp then mf = "m" end
      end
    elseif c == 2 then
      if uid.s.av then

      else
        drawMenuDrawer("Setting unavailable", {"Ok"}, {title="Settings > Visuals",choices={"Back", "Theme", "Sonic Mania Mode " .. (uid.v.sm and "[ON] " or "[OFF]")}})
      end
    elseif c == 3 then
      if uid.v.sm then uid.v.sm = false else uid.v.sm = true end
    end
  end
end
-- enables whether to show sonic selection flashes just like in sonic mania and enable the sounds
function options.enableSonicMode(bool)
  if type(bool) == "boolean" then settings.visuals.sonicMode = bool else error("must be tuue or false") end
end

--<#END#OF#SETYINGS#>--

function drawMenu(title, ...)
	local args = {...}
	local options
	if type(args[1]) == "table" then
		options = args[1]
	else
		options = {...}
	end
	cursor = 1
	count = timerCount
	countDownTimer = nil
	if count then
		countDownTimer = os.startTimer(1)
	end
	if cursorHighlight then
		tmp = selectedColor
		tmp2 = txtColor
		dist = n-cursor
	for i = 1,20 do
		cursor = cursor + (dist/20)
		draw(title, options, menuExtraText)
		sleep(0.05)
	end
	for i = 1, 2 do
		selectedColor = tmp
		draw(title, options, menuExtraText)
		sleep(0.4)
		selectedColor = tmp2
		draw(title, options, menuExtraText)
		sleep(0.4)
	end
	selectedColor = tmp
	draw(title, options, menuExtraText)
end

while true do
  draw(title, options, menuExtraText)
  event, key = os.pullEvent()
  if event == "key" then
  if(key == keys.up) or (key == keys.w) then
    cursor = cursor-1
  end
  if(key == keys.down) or (key == keys.s) then
    cursor = cursor+1
  end
  if(key == keys.enter) then
    for i = 1, 10 do
      selectedColor = tmp
      draw(title, options, menuExtraText)
      sleep(0.05)
      selectedColor = tmp2
      draw(title, options, menuExtraText)
      sleep(0.05)
    end
    selectedColor = tmp
    draw(title, options, menuExtraText)
    term.setBackgroundColor(colors.black)
    term.setTextColor(colors.white)
    term.clear()
    term.setCursorPos(1, 1)
    break
  end
  end
  if event == "timer" and key == countDownTimer then 
    count = count - 1
    if count < 0 then
      break
    end
  end
  if(cursor > #options) then cursor = #options end
  if(cursor < 1) then cursor = 1 end
end

if count then
  os.cancelTimer(countDownTimer)
end

return cursor
end
