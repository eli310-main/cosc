--How do I use this?
--please tell me

--[[

DOCUMENTATION:

daemon.add(function[, error])
- starts a function in the background which will loop until the function reaches its end
- error is a function that will be ran if the coroutine ends, it is passed the error code 

daemon.rem(table)
- removes a currently running coroutine
- pass it the table returned by daemon.add

daemon.getBackground()
- returns a table of all coroutines currently in the background as well as the event they are waiting for to continue running

]]


--a stateless iterator for use in for loops.
local function fIter (t, i)
	i = not i and #t or (i - 1)
	if i>0 then
		return i, t[i]
	end
end

--[[a table that stores all running daemons
when we add a new value we first create a coroutine using the fFunc passed]]
local tBackground = setmetatable({},{
	__newindex = function(t, k, v)
		v.cRoutine=coroutine.create(v[1])
		rawset(t, k, v)
	end;
})

--[[starts a new daemon

fFunc = function that runs in background
fError = function ran if fFunc ever ends for any reason

returns identifier for new daemon
]]
function add(fFunc, fError)
	tBackground[#tBackground + 1] = {fFunc, fError=fError}
	return tBackground[#tBackground]
end

--[[removes a running daemon
internally this sets a flag telling the system to remove this daemon next time it loops through

cRoutine = daemon to remove
]]
function rem(cRoutine)
	cRoutine.sFilter="Daemon_internal_remove"
end

--returns identifiers for all running daemons
function getBackground()
	return tBackground
end

--[[internal variable
determines what level of depth we are in the daemon]]
local bPullMeta = false


--[[replacing the native os.pullEventRaw to function]]
function os.pullEventRaw(sEvent)
	local tData

	if bPullMeta then
		return coroutine.yield(sEvent)
	else
		repeat
			bPullMeta = true
			tData = {coroutine.yield()}

			for iIndex, cRoutine in fIter, tBackground do --
				if cRoutine.sFilter==tData[1] or not cRoutine.sFilter then
					local bOk, sInnerEvent = coroutine.resume(cRoutine.cRoutine, unpack(tData))
					if bOk then
						cRoutine.sFilter = sInnerEvent
                    else
                        table.remove(tBackground, iIndex)
                        if cRoutine.fError then
                            cRoutine.fError(sInnerEvent, cRoutine)
                        end
					end
				elseif cRoutine.sFilter=="Daemon_internal_remove" then
					table.remove(tBackground, iIndex)
				end
			end
			bPullMeta = false
		until tData[1] == sEvent or not sEvent
		return unpack(tData)
	end
end
