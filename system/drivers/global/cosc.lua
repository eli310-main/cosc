function getUser(usrname)
    return _G.udb[usrname]
end
function getUsers()
    return _G.udb
end
function getUsernames()
    r = {}
    for n,v in pairs(_G.udb) do
        r[#r+1] = n
    end
    return r
end
function createUser(n,p)
    _G.udb[n] = {n,p}
    cosc.commitUDB()
end
function commitUDB()
    reg.users = _G.udb
    cosc.saveReg("users")
end
function saveReg(n)
    rf = fs.open("/system/config/"..n,"w")
    rf.write(textutils.serialise(reg[n]))
    rf.close()
end
function deleteUser(n)
    _G.udb[n] = nil
    cosc.commitUDB()
end
function run(p)
    local lreg = reg
    local preg = reg
    preg.users = nil
    parallel.waitForAny(function()
        _G.exit = true
        while exit do
            sleep(0.1)
            if not (lreg == reg) then
                if not (lreg.system == reg.system) then
                    savsystem = true
                end
                if not (lreg.users == reg.users) then
                    resusers = true
                end
                if not (lreg.software == reg.software) then
                    savsoftware = true
                end
            end
        end
    end,function()
        os.run({reg = preg},p)
    end)
end
function getVersion()
    return {
        version = {1, 0, 0},
        codename = "Neptune",
        copyright = {2020, 2021},
        edition = "", -- Basically "" is normal while others like Education and Pro or even Home editions are good for the OS.
    }
end
