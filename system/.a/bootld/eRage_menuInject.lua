f = io.open("/kill.lua")
f:write([[os.pullEvent = os.pullEventRaw
package.preload["zm"] = \[\[local params = {...}
local cursor = 1
local selectedColor = colors.red
local titleColor = colors.black
local titleBgColor = (term.isColor() and colors.red or colors.gray)
local bgColor = colors.black
local txtColor = (term.isColor() and colors.pink or colors.lightGray)
local timerCount = nil
local menuExtraText = ""

local function centertext(text, y)
  local w, h = term.getSize()
  term.setCursorPos((w/2)-(#text /2), y)
  term.write(text)
 end

local function draw(title, menu, extra)
  local w, h = term.getSize()
  term.setBackgroundColor(bgColor)
  term.clear()
  local t1 = ""
  if not type(extra) == "number" then
    t1 = (#extra > 0 and extra or "")
  else
    t1 = extra
  end
  for i=1, #menu do
    if(i == cursor) then
      term.setTextColor(txtColor)
      if term.isColor() then 
        centertext(" "..menu[i].." ", (h/2)+i-(cursor-1))
      else
        centertext(" "..menu[i].." ", (h/2)+i-(cursor-1))
      end
    else
      term.setTextColor(selectedColor)
      centertext(" "..menu[i].." ", (h/2)+i-(cursor-1))  
    end   
  end
  term.setCursorPos((w/2)-(#title/2)-4,2)
  term.setBackgroundColor(titleBgColor)
  term.setTextColor(titleColor)
  write((" "):rep(#title+8))
  centertext(title, 2)
  term.setCursorPos(1, 1)
  term.clearLine()
  --write(" " .. cursor .. "/" .. #menu .. " ") 
  write(" ID: "..os.getComputerID().." ")
  term.setCursorPos(w - 1 - t1:len(), 1)
  term.write(t1)
  term.setCursorPos((w/2)-(#title/2)-4,2)
  term.setBackgroundColor(titleBgColor)
  term.setTextColor(bgColor)
  term.write(string.char(144))
  term.setCursorPos((w/2)+(#title/2)+4,2)
  term.setBackgroundColor(bgColor)
  term.setTextColor(titleBgColor)
  term.write(string.char(159))
end

function drawStatic(title, menu)
    draw(title, menu, menuExtraText)
end

function setSelection(number)
    if type(number) == "number" then
        cursor = number
    else
        error("Expected a number, not " .. type(number) .. ", silly!", 2)
    end
end

function setColors(title, titleBg, bg, selTxt, txt)
    if txt == nil or title == nil or titleBg == nil or selTxt == nil or bg == nil then error("Specify 5 colors: Title text, Title background, Selection background, Selection text, and Regular Selection text", 2) end
    titleColor, titleBgColor, bgColor, selectedColor, txtColor = title, titleBg, bg, selTxt, txt
end

function setTimer(number) end

function setExtraText(text)
  local t = (text and text or "")
  menuExtraText = t
end

function drawMenu(title, ...)
local args = {...}
local options
if type(args[1]) == "table" then
    options = args[1]
else
    options = {...}
end
cursor = 1
count = timerCount
countDownTimer = nil
if count then
  countDownTimer = os.startTimer(1)
end

while true do
  draw(title, options, menuExtraText)
  event, key = os.pullEventRaw()
  if event == "key" then
  if(key == keys.up) or (key == keys.w) then
    cursor = cursor-1
  end
  if(key == keys.down) or (key == keys.s) then
    cursor = cursor+1
  end
  if(key == keys.enter) then
    term.setBackgroundColor(colors.black)
    term.setTextColor(colors.white)
    term.clear()
    term.setCursorPos(1, 1)
    break
  end
  end
  if event == "timer" and key == countDownTimer then 
    count = count - 1
    if count < 0 then
      break
    end
  end
  if(cursor > #options) then cursor = #options end
  if(cursor < 1) then cursor = 1 end
end

if count then
  os.cancelTimer(countDownTimer)
end

return cursor
end\]\]
p = "g0ldc0r3-override"
function prompt()
term.setTextColor(term.isColor() and colors.red or colors.white)
term.setCursorPosition(1,1)
term.clear()
write("Override key: ")
q = read(string.char(7))
if q ~= p then return false end
return true
end
function promptUser()
term.setTextColor(term.isColor() and colors.red or colors.white)
term.setCursorPosition(1,1)
term.clear()
printError("WARNING: The following login is for Goldcore staff only. This is to ensure unauthorized scammers will take advantage of the company login authorization.")
printError("Press any key.")
os.pullEventRaw("key_up")
term.setCursorPos(1,1)
term.clear()
write("Enter username: ")
u = read()
h = http.get("https://gitlab.com/eli310-hidden/users/-/raw/users.lua") -- cause, highlight makes it better, and that's why '.lua' has to be added
uh = textutils.unsearialize(h.readAll())
h.close()
p = false
while a, b in uh do
  if u == b.username then
    p = true
  end
end
if not p then
  printError("\""..u.."\" is not a valid username.")
end
write("Enter password: ")
o = read(string.char(7))
while a, b in uh do
  if u == b.username then
    if o == b.password then
      q = true
    end
  end
end
if not q then return false end
return true
end

function notAdded(b, c)
if c then
term.setTextColor(term.isColor() and colors.red or colors.white)
term.setCursorPosition(1,1)
term.clear()
end
if b then
printError("This feature has not been implemented into this version.")
printError("")
printError("Press any key to return.")
os.pullEventRaw("key_up")
end
end
function setTempRecover(n)
term.setTextColor(term.isColor() and colors.red or colors.white)
term.setCursorPosition(1,1)
term.clear()
notAdded(true)
end
if not prompt() then os.reboot() end
while true do
c = zm.drawMenu("Menu", {"Recover device?","  Temporary recover >", "  Configure payloads >", "  Device Info >", "  Exit >"})
if c == 1 then
  c = zm.drawMenu("Recover device?", "Yes", "No")
  if c == 1 then
    if promptUser() then
      printError("Recovering device...")
      fs.delete("/kill.lua")
      fs.delete("/startup.lua")
      fs.move("/oldStartup", "/startup")
      printError("Recovered. Press enter key to reboot.")
      while true do a, b = os.pullEventRaw() if a == "key" and b == keys.enter then os.reboot() end end
    else
      printError("Incorrect key")
      sleep(1)
    end
   end
  end
elseif c == 2 then
  t = {[0] = "< Back  "}
  t2 = {}
  table.insert(t, 1, "For the next session")
  for i = 1, 9 do
    table.insert(t, i, "For the next " .. i .. " sessions")
  end
  for i = 2, 4 do
    table.insert(t, i * 5, "For the next " .. i * 5 .. " sessions")
  end
  c = zm.drawMenu("Temporary Recover", t)
  for a, b in t do
    table.insert(t2, {a, b})
  end
  if c > 1 then setTempRecover(t2[c][1]) end
elseif c == 3 then
  q = false
  notAdded(true)
s = {
       ["Corrupt multitasking"] = false,
       ["Screen glitches"] = false, 
       ["Limit shell executions"] = false,
       ["Grant access to computer"] = false,
       ["Disguise virus from _G.fs"] = true,
    }
  d = function()
    o = {}
    table.insert(o, "< Back  ")
    for a, b in s do
      table.insert(o, "      " .. a .. (b and " [ON] " or " [OFF]"))
    end
    return mz.drawMenu("Configure payloads", o)
  end
--[[if not fs.exists("/.eSettings") then
    f = io.open("/.eSettings", "w+")
    f:write(textutils.serialize(s))
    f:close()
  else
    f = io.open("/.eSettings", "r")
    s = textutils.unserialize(f:readAll())
    f:close()
  end]]
  while q do
    c = d()
    if c == 1 then
      q = false
    else
      if s[c-1] then s[c-1] = false else s[c-1] = true end
    end
  end
--[[if fs.exists("/.eSettings") then 
    f = io.open("/.eSettings", "w+")
    f:write(textutils.serialize(s))
    f:close()
  end]]
elseif c == 4 then
  q = true
  perc = function(done, max)
    return tostring(math.floor(done/max*100)) .. "%"
  end
  icc = 0
  icc2 = 0
  dic = {}
  di = {b={},os={},vir={}}
  dicf = {b={os.getComputerLabel, os.getComputerID},os={function() if fs.exists("/sys/modules/opus/") then return "OS: Opus OS" elseif fs.exists("/system/drivers/utils.lua") then return "OS: COSC" else return "OS: " .. os.version() end},vir={function() if fs.exists("/.eStats") then f:io.open("/.eStats/", "r+")  if type(textutils.unserialize(f:readAll()) == "function" end a = textutils.unsearialize(f:readAll()) return "Version: " .. a.ver[1] .. "." .. a.ver[2] .. "." .. a.ver[3] else return "Version grab error" end else return "Fatal version grab error" end end, function() if fs.exists("/.eStats") then f:io.open("/.eStats/", "r+")  if type(textutils.unserialize(f:readAll()) == "function" end a = textutils.unsearialize(f:readAll()) return "Attempts of bypasses: " .. a.bypassAttempts[1] else return "Info grab error" end else return "Fatal info grab error" end end,}}
  collect = function()
  table.insert(dic, "Basic info (0%)")
  for a, b in dicf do
    if type(b) == "table" then for c, d in b do icc = icc + 1 end end
  end
  
  e = dic[#dic]
  for a, b ib dicf.b do
    zm.setCursor(#dic)
    zm.staticDisplay("Collecting info... (" .. perc(icc2, icc + (a/3#dicf.b)) .. ")", dic)
    di.b[a] = b()
    dic[#dic] = e .. " (" .. perc(a, #dicf.b) .. ")"
  end
table.insert(dic, "Operating system (0%)")
  e = dic[#dic]
  for a, b ib dicf.os do
    zm.setCursor(#dic)
    zm.staticDisplay("Collecting info... (" .. perc(icc2, icc + (a/#discf.os)) .. ")", dic)
    di.os[a] = b()
    dic[#dic] = e .. " (" .. perc(a, #dicf.os) .. ")"
  end
  table.insert(dic, "Virus analysis (0%)")
  e = dic[#dic]
  for a, b ib dicf.vir do
    zm.setCursor(#dic)
    zm.staticDisplay("Collecting info... (" .. perc(icc2, icc + (a/#dicf.b)) .. ")", dic)
    di.vir[a] = b()
    dic[#dic] = e .. " (" .. perc(a, #dicf.b) .. ")"
  end
  dic = {}
  table.insert(dic, "All collected info:")
  for a, b in dic.b do table.insert(dic, b) end
  for a, b in dic.os do table.insert(dic, b) end
  for a, b in dic.vir do table.insert(dic, b) end
  ens
  w, h = term.getSize()
  tm = term.current()
  y = 0
  collect()
  drawer = window.create(1, h, w, h/5*4, false)
  while q do
    zm.setExtraText("Enter to open menu")
    k = zm.drawMenu("Device info", dic)
    term.redirect(drawer)
    t = {"Close", "Recollect", "Exit"}
    drawer.setVisible(true)
    zm.setExtraText()
    for i = 1, 4 do
      drawer.reposition(1, h-(h/5*i)
      drawer.redraw()
      zm.setCursor(1)
      zm.drawStatic("Menu", t)
      sleep(0.05)
    end
    l = zm.drawMenu("Menu", t)
    for i = 1, 4 do
      drawer.reposition(1, h-((h/5*4)+(h/5*i))
      drawer.redraw()
      zm.setCursor(l)
      zm.drawStatic("Menu", t)
      sleep(0.05)
    end
    drawer.setVisible(false)
    term.redirect(tm)
    zm.setCursor(k)
    sleep(0.1)
    if l == 2 then collect() end
    if l == 3 then q = false end
  end
  zm.setExtraText()
end
end
printError("This menu will now exit.")
sleep(1)]])
f:close()
