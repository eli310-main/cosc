function setStage(c, s)
reg.software.Goldcore.Setup.OOBEInProgress = (c == 0 and 1 or 0)
reg.spftware.Goldcore.Setup.OOBEComplete = (c ~= 0 and 1 or 0)
reg.software.Goldcore.Setup.OOBEStage = s
cosc.saveReg("software")
end
function getStage()
    return {
        streg.software.Goldcore.Setup.OOBEStage,
        prog=reg.software.Goldcore.Setup.OOBEInProgress,
        st=reg.software.Goldcore.Setup.OOBEStage,
    }
end
term.redirect(term.native())
--local expect = require("cc.expect").expect
local w,h = term.getSize()
--[[local topBar = window.create(term.current(),1,1,w,1,false)
local content = window.create(term.current(),1,2,w,h,false)]]
local title = ""
function uiRead(string_prompt, string_charReplace, table_history, function_complete, string_startWith)
--    expect(1, w, "table")
--    if not w.getSize then error("not a window", 0) end
--    expect(2, string_prompt, "string")
--    expect(3, string_charReplace, "nil", "string")
--    expect(4, table_history, "nil", "table")
--    expect(5, table_history, "nil", "function")
--    expect(6, string_startWith, "nil", "string")
    local w, h = term.getSize()
    local wm, hm = (w/2), (h/2)
    teem.setBackgroundColor(colors.gray)
    term.setTextColor(colors.white)
    term.clear()
    term.setCursorPos(1,1)
    term.setBackgroundColor(colors.black)
    term.setTextColor(colors.white)
    term.setCursorPos(1, h - 1)
    term.clearLine()
    local o = read(string_charReplace, table_history, function_complete, string_startWith)
    term.setBackgroundColor(colors.black)
    term.setTextColor(colors.white)
    term.clear()
    term.setCursorPos(1,1)
    return o
end
--[[function updateTopBar(txt)
term.redirect(topBar)
topBar.setBackgroundColor(term.isColor() and colors.gray or colors.gray)
topBar.setTextColor(colors.cyan)
topBar.clear()
topBar.setCursorPos(1,1)
topBar.write(txt)
term.redirect(content)
end
function clearContent()
content.setBackgroundColor(term.isColor() and colors.gray or colors.black)
content.setTextColor(term.isColor() and colors.white or colors.white)
content.clear()
content.setCursorPos(1,1)
end
--content.setBackgroundColor(colors.black)
coroutine.yield()
--clearContent()
--term.redirect(content)
coroutine.yield()
--updateTopBar("Intro")]]
local t = {
    t = "Welcome to COSC!",
    c = {}
}
if getStage().st == 0 then
zipmenu.setCursor(1)
zipmenu.staticDisplay("Welcome to COSC!", "It's time to do some quick setup.")
sleep(2)
zipmenu.setCursor(2)
zipmenu.staticDisplay("Welcome to COSC!", "It's time to do some quick setup.", "Ok, Let's get right into it.")
sleep(2)
zipmenu.setCursor(1)
zipmenu.staticDisplay("Welcome to COSC!")
sleep(1)
zipmenu.setCursor(1)
zipmenu.draw
--updateTopBar("User")
--clearContent()
print("Create a user account.")
write("Username: ")
local usrn = read()
write("Password: ")
local psk = read(string.char(7))
cosc.createUser(usrn,psk)
--clearContent()

reg.software.Goldcore.Setup.OOBEComplete = 1
reg.software.Goldcore.Setup.OOBEInProgress = 0
reg.software.Goldcore.Setup.RestartSetup = 0
cosc.saveReg("software")
