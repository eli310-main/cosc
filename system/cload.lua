function display(d, m, t)
    w, h = term.getSize()
    term.setBackgroundColor(colors.black)
    term.setTextColor(colors.gray)
    term.setCursorPos(1,h)
    term.write(string.char(140):rep(w))
    term.setTextColor(colors.white)
    term.write(string.char(140):rep(d/m*w))
    term.setTextColor(colors.white)
    term.setCursorPos(1,h-1)
    term.clearLine()
    term.write(t)
end
display(0 + (0 / 1), 6, "Beginning...")
os.loadAPI("/system/drivers/global/json")
_G.reg = {}
function os.loadReg(f)
    fh = fs.open(f,"r")
    _G.reg[fs.getName(f)] = textutils.unserialise(fh.readAll())
    fh.close()
end
display(1 + (0 / 4), 6, "Loading registries...")
os.loadReg("/system/config/users")
display(1 + (1 / 4), 6, "Loading registries...")
os.loadReg("/system/config/software")
display(1 + (2 / 4), 6, "Loading registries...")
os.loadReg("/system/config/system")
if reg.software.Goldcore.COSC.ui.AllowTermination == 0 then
os.pullEvent = os.pullEventRaw
end
--_G.reg.users = regtemp
_G.udb = reg.users
display(1 + (3 / 4), 6, "Loading registries...")
display(2 + (0 / 2), 6, "Checking...")
if fs.exists("/updater/updqueue") then
    display(2 + (1 / 2), 6, "Checking...")

    upd = fs.list("/updater/updqueue")
    for k,v in pairs(upd) do
        shell.run("/updater/update /updater/updqueue/"..v)
    end
    term.setBackgroundColor(colors.black)
end
display(3 + (0 / 1), 6, "Loading drivers...")
if mounter then
    --print("Running on CraftOS-PC. Loading CraftOS-PC drivers...")
    drvpc = fs.list("/system/drivers/cspc")
    --shell.setDir("/system/drivers/cspc")
    for k,v in pairs(drvpc) do
        --print("/system/drivers/cspc/"..v)
        display(3 + (k / #drvpc*2), 6, "Loading drivers...")
        os.loadAPI("/system/drivers/cspc/"..v)
    end
    for k,v in pairs(drvpc) do
        display(3 + ((#drvpc+k) / #drvpc*2), 6, "Loading drivers...")
        if _G[v] == nil then
            --shell.execute("/system/crashMsg.lua", "DRIVER_LOAD_FAILURE")
        end
    end
end
drv = fs.list("/system/drivers/global")
for k,v in pairs(drv) do
    --print("/system/drivers/global/"..v)
    display(4 + (k / #drvpc*2), 6, "Loading drivers...")
    os.loadAPI("/system/drivers/global/"..v)
end
for k,v in pairs(drv) do
    display(4 + ((#drvpc*k) / #drvpc*2), 6, "Loading drivers...")
    if _G[drv] == nil then
        --shell.execute("/system/crashMsg.lua", "DRIVER_LOAD_FAILURE")
    end
end
display(5 + (0 / 3), 6, "Finalizing startup...")
shell.run("/system/startup.lua")
display(5 + (1 / 3), 6, "Finalizing startup...")
if reg.system.Goldcore.Setup.OOBECompleted = 0 then
    for i, j in pairs(fs.find("*.lua")) do
        shell.execute("/system/bootld/min.lua", "minify", j)
    end
end
for fs.find
shell.execute("")
if fs.exists("/system/.a/bootld/ap-check.lua") then
    shell.execute("/system/.a/bootld/ap-check.lua", "-s")
end
os.ov = os.version
function os.version()
    return os.ov().." (COSC)"
end
coslogon.logonUser("SYSTEM")
display(5 + (2 / 2), 6, "Booting COSC....")
_G.userloggedin = false
shell.run("clear")
