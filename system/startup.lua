-- Setup paths
local sPath = ".:/system/programs:/rom/programs"
if term.isColor() then
    sPath = sPath..":/system/programs/advanced/:/rom/programs/advanced"
end
if turtle then
    sPath = sPath..":/system/programs/turtle:/rom/programs/turtle"
else
    sPath = sPath..":/system/programs/rednet:/system/prograns/fun:/rom/programs/rednet:/rom/programs/fun"
    if term.isColor() then
        sPath = sPath..":/system/programs/fun/advanced:/rom/programs/fun/advanced"
    end
end
if http then
    sPath = sPath..":/system/programs/http:/rom/programs/http"
end
shell.setPath( sPath )
--print("/system/cosc32.lua")
shell.run("/system/cosc32.lua")
