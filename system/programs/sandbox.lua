local yellowbox
do
  local fnFile, e = loadfile("/system/sandbox/yellowbox.lua", nil, _ENV)
  if fnFile then
    yellowbox = fnFile()
  else
    error(e)
  end
end
local tArgs = {...}
local file = fs.open("bios.lua", "rb")
local files = {}
local vfsf = fs.open("/system/sandbox/files.vfs","r")
local files = textutils.unserialize(vfsf.readAll())
vfsf.close()
if tArgs[1] == "install" then
files["startup.lua"] = "shell.run(\"/realsys/system/sandbox/installer.lua\")\nos.shutdown()"
end
local vm = yellowbox:new(file.readAll())
file.close()
vm:loadDiskTable("/system/sandbox/files.vfs",nil,files)
if tArgs[1] == "install" then
vm:mount("/realsys","/")
end
vm:resume()
while vm.running do
	vm:queueEvent(os.pullEventRaw())
	vm:resume()
end
