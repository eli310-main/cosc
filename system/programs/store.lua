ndenv.exit()
print("Loading...")
local gget = http.get("https://gitlab.com/api/v4/projects/24334029/repository/tree?path=apps")
local ggetd = textutils.unserialiseJSON(gget.readAll())
gget.close()
zm = dofile("/system/drivers/global/zipmenu.lua")
st = dofile("/system/drivers/global/stringutils.lua")
function uisc(...)
    shell.execute("/system/uisc", ...)
end
function menu(c)
if c == 1 then
zm.drawMenu("COSC Update", st.wrap("A system update is required to continue using the atore."))
return false, "update_http"
end
sel = zm.drawMenu("Menu", {"Apps", "Libraries", "Games", "Installed", "Store Protect"})
return true, sel
end
function appList(h)
if type(h) == "number" then zm.setHighligjt(h+1) end
local mtb = {}
mtb[#mtb+1] = "(<) Open Menu    "
for k,v in ipairs(ggetd) do
    mtb[#mtb+1] = v.name
end
q = false
while not q do
    local sel = zm.drawMenu("Store",mtb)
    zm.setCursor(1)
    if not sel == 1 then
    conf = zm.confirmAction({title="Store",contents,mtb}, 3, "Install app?")
    zm.setCursor(sel)
    zm.drawMenuDrawer("App install", {"This will be installed.", "App: "..mtb[sel], "Licened by Goldcore and XenusSoft", {title="Store",contents,mtb})
    if conf then
    uisc("Installing "..mtb[sel].."...")
    local ggetf = http.get("https://gitlab.com/eli310-main/cosc-store/-/raw/master/apps/"..mtb[sel].."/install.lua")
    if not ggetf then
        w,_=term.getSize()
        zm.setCursor(sel)
        zm.drawMenuDrawer("Error", st.wrap("\"install.lua\" could not be found on the repo.", w-2), {title="Store",contents=mtb})
        
    else
        err, err2 = pcall(load(ggetf.readAll()))
        if not err then
            zm.drawMenuDrawer("Error", st.wrap("Installer crashed. Details: "..err2, w-2), {title="Store",contents=mtb})
        else
            zm.drawMenuDrawer("Success!", st.wrap("The app "..mtb[sel].." was successfully installed. Check it out on the apps list on your app browser!", w-2), {title="Store",contents,mtb})
        end
    end
    ggetf.close()
    else
        zm.setCursor(sel)
        zm.drawMenuDrawer("We got concerns.", st.wrap("Change your mind? Is it spyware? Is it a virus? Tell us by submitting a issue to our official website: https://eli310hub.tribe.so/"), w-2), {title="Store",contents,mtb})
    end
    else
        q == true
    end
end
end
mq = false
while not mq do
    sel = menu()
    if sel == 1 then
        appList()
    else
        uisc("Error while opening section...", "Feature not added.", "Check back soon!")
        os.pullEventRaw()
    end
end

local w,h = term.getSize()
ndenv.gowin(w)
term.clear()
term.setCursorPos(1,1)
