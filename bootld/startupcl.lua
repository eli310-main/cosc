local tArgs = {...}
-- had to remove the bcos thing because it crashed the system on startup - OMGasdV
if tArgs[1] == "debug" then --bcos api for BCOS project, see in shared projects tab at Goldcore group home page
	debugm = true
end
shell.run("clear")
package.path = "/?;/?.lua;/?/init.lua;/rom/modules/main/?;/rom/modules/main/?.lua;/rom/modules/main/?/init.lua;/system/drivers/library/?.lua;/system/drivers/library/?/init.lua;/system/drivers/library/?" -- So that the require() function can direct to the system libraries as well as the rom directories without using the system.drivers all the time.
local succ, err = pcall(function()
if not fs.exists("/bootld/supportKey") then
   local key = ""
   local char = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklnmopqrstuvwxyz01234567890"
    for j = 1, 4 do
      for i = 1, 5 do
          a = math.random(0, #char-1)
          key = key..string.sub(char, a, a+1)
      end
      key = key .. "-"
    end
    for i = 1, 5 do
        a = math.random(0, #char-1)
        key = key..string.sub(char, a, a+1)
    end
    f=io.open("/bootld/supportKey", "w")
    f:write(key)
    f:close()
end
--shell.run("bootmgr")
--print("Loading files...")
--print("startup.lua")
--print("/system/cload.lua")
--shell.run("/system/crashMsg CRITICAL_PROCESS_DIED")
--if debugm then
--	shell.run("/bootld/debuganim.lua")
--else
	shell.run("/bootld/anim.lua")
--end
shell.run("clear")
if reg.system.Goldcore.COSC.BootMode == "kernel" then
    print("Booting in kernel mode.")
    shell.run(reg.system.Goldcore.COSC.KernelBootCMDLine)
    print("Rebooting.")
    reg.system.Golcore.COSC.BootMode = "shell"
    os.reboot()
end
if reg == nil then
print("The system could not start cload. You must reinstall COSC.")
while true do
sleep(2)
end
end
if reg.software.Goldcore.Setup.RestartSetup == 1 then
reg.software.Goldcore.Setup.OOBEComplete = 0
reg.software.Goldcore.Setup.OOBEInProgress = 0
end
if (reg.software.Goldcore.Setup.OOBEComplete == 0) and (reg.software.Goldcore.Setup.OOBEInProgress == 0) then
shell.run(reg.software.Goldcore.Setup.CMDLine)
elseif reg.software.Goldcore.Setup.OOBEInProgress == 1 then
printError("The system rebooted while OOBE was in progress. You must reinstall COSC.")
sleep(2)
printError("Press any key to reinstall.")
os.pullEventRaw("key_up")
term.setTextColor(colors.red)
d = fs.list("/")
print("")
_, y = term.getCursorPos()
for i, j in pairs(d) do
if j ~= ".settings" and j ~= "rom" and j:sub(1,4) ~= "disk" then
fs.delete("/"..j)
end
term.setCursorPos(1,y)
term.clearLine()
term.write("Formatting HDD... "..math.floor(j/d*100).."% ("..fs.getSize("/").."/"..fs.getFreeSpace("/")+fs.getFreeSpace("/")..")")
end print("")
shell.run("wget https://gitlab.com/eli310-main/cosc/-/raw/master/installer.lua /startup.lua")
end
term.redirect(term.native())
function userenv()
--print("/system/programs/shell.lua")
shell.run("/system/ui/logonui")
--shell.run("/system/crashMsg SHELL_DIED")
shell.run("/system/userinit")
end
function 
servicenetname()
netname.init()
while true do
netname.run()
end
end
--parallel.waitForAny(userenv,servicenetname)
while true do
userenv()
end
end)
if not succ then
    printError("System failed."..(http and "Error reported to devs." or ""))
    if http then
        http.post("https://gitlab.com/api/v4/projects/22651806/issues?title="..textutils.urlEncode("SYSTEM ERROR").."&labels=Bug,system.cload&descrdescription="..textutils.urlEncode("ERROR: ["..err.."]\nTYPE: Critical\n\nThe file where it's located is shown at the error."), "PRIVATE-TOKEN: m6HoU22-Mn7--yBWwAtN")
    end
end
