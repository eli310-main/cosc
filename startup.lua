-- Debug mode if needed
local debugm = true

print("Starting up...")
shell.run("set shell.allow_disk_startup false")
print("Disabled disk startup")
at = os.pullEvent
os.pullEvent = os.pullEventRaw
print("Disabled forceful termination")
term.clear()
term.setCursorPos(1,1)
print("CraftBIOS 210121b")
print("")
--print("Press any key for Options")
--print("")
print("Finding OS...")
if fs.exists("/bootld/startupcl.lua") then
    if fs.exists("/system/cload.lua") then
        print("Found COSC.")
        secureboot = true
    else
        print("Found startup chainload.")
        secureboot = false
    end
    if secureboot then
        print("Booting (COSC)...")
        sleep(1)
        os.pullEvent = at
        local succ, err
		if debugm then
			succ, err = pcall(shell.run,"/bootld/startupcl.lua debug")
		else
			succ, err = pcall(shell.run,"/bootld/startupcl.lua")
        end
        if not succ then
            printError("System failed."..(http and "Error reported to devs." or ""))
            if http then
                http.post("https://gitlab.com/api/v4/projects/22651806/issues?title="..textutils.urlEncode("[FATAL] SYSTEM BOOT ERROR").."&labels=Bug,system.boot&descrdescription="..textutils.urlEncode("ERROR: ["..err.."]\nTYPE: Fatal\n\nThe file where it's located is shown at the error."), "PRIVATE-TOKEN: m6HoU22-Mn7--yBWwAtN")
            end
            while true do read(" ") end
        elseif not utils then
            print("This is not COSC!")
            sleep(3)
			if debugm then
				while true do
					sleep(1)
				end
            else
				os.reboot()
			end
        end
        utils.system().crash("SYSTEM_UNSAFE_SHUTDOWN")
    else
        printError("The booting process is unsecure, please be advised that it's currently booting in a different way.")
        write("Press any key...")
        os.pullEventRaw()
        _, y = term.getCursorPos()
        term.clearLine()
        term.setCursorPos(1, y)
        print("Booting...")
        sleep(1)
        os.pullEvent = at
        shell.run("/startupcl.lua")
    end
else
    print("No OS found.")
    print("Booting in normal mode.")
    sleep(1)
    term.clear()
    term.setCursorPos(1,1)
    os.pullEvent = at
    while true do
        term.setCursorPos(1,1)
        term.clear()
        term.setTextColor(colors.gray)
        print("To open up power options, terminate the shell.")
        shell.run("shell")
        term.setTextColor(colors.white)
        term.setBackgroundColor(colors.black)
        term.setCursorPos(1,1)
        print("Choose a power option")
        print("S=Shutdown")
        print("R=Reboot")
        print("C=Reinstall COSC")
        write("Response: ")
        q = true
        os.pullEvent = os.pullEventRaw
        while q do
            p1, p2, p3, p4, p5 = os.pullEvent()
            if p1 == "key" then
                if p2 == keys.s then
                    printError("Shutdown")
                    sleep(1)
                    os.shutdown()
                elseif p2 == keys.r then
                    printError("Reboot")
                    sleep(1)
                    os.reboot()
                elseif p2 == keys.c then
                    print("Reinstall COSC")
                    printError("Failed: Not yet implemented as a feature... Try again soon.")
                    os.pullEvent()
                    os.reboot()
                end
            end
        end
    end
end
os.reboot()